﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour
{
    private Rigidbody2D playerRigidbody2D;

    //Variable to track how much movement is needed from input.
    private float movePlayerVector;

    //Determining which way the player is facing.
    private bool facingRight;

    public float MoveSpeed = 4.0f;

    void Awake()
    {
        //Getting the reference.
        playerRigidbody2D = (Rigidbody2D) GetComponent(typeof (Rigidbody2D));
    }

    void Update()
    {
        //Control the movement.
        ControlMovement();

        //Control the facing.
        ControlFacing();
    }

    private void ControlMovement()
    {
        //Getting the horizontal Input.
        movePlayerVector = Input.GetAxis("Horizontal");

        playerRigidbody2D.velocity = new Vector2(movePlayerVector * MoveSpeed, playerRigidbody2D.velocity.y);

    }

    private void ControlFacing()
    {
        if (movePlayerVector > 0 && !facingRight)
        {
            Flip();
        }
        else if (movePlayerVector < 0 && facingRight)
        {
            Flip();
        }
    }

    private void Flip()
    {
        //Switch the way the player is labeled as facing.
        facingRight = !facingRight;

        //Multiply the local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
