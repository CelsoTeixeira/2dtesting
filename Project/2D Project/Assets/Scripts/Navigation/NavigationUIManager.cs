﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*  Attach this to the NavigationPanel main object!
 *  
 *  Make sure to put the Buttons Methods in the buttons on the editor! TODO: There is anyway yo add a OnClick on the button from the script ? Check it!
 */

public class NavigationUIManager : MonoBehaviour
{
    #region Singleton Pattern
    //Simple singleton setup, we only need one instance of this.
    public static NavigationUIManager Instance { get; private set; }

    private void SingletonSetUp()
    {
        if (Instance != this && Instance != null)
            Destroy(this.gameObject);

        Instance = this;

        DontDestroyOnLoad(this.gameObject);
    }

    #endregion

    private string TravelDestination;
    private Text text;

    #region Mono
    void Awake()
    {
        SingletonSetUp();

        //Just get our text component, make sure the Text is the first gameObject off the childrens.
        if (text == null)
        {
            text = GetComponentInChildren<Text>();    
        }
        
        //DeActive this if its actived.
        this.gameObject.SetActive(false);

    }
    #endregion

    //We change the text here, basicaly the same stuff we have done in the OnGUI function.
    public void SetUpText(string destination)
    {
        text.text = "Do you want to travel to " + NavigationManager.GetRouteInfo(destination) + "?";
        SetUpDestination(destination);  //Changing the destination.
    }

    //We change the destination here!
    private void SetUpDestination(string destination)
    {
        TravelDestination = destination;
    }

    #region Button Helper

    public void TravelButton()
    {
        NavigationManager.NavigateTo(TravelDestination);
    }

    public void DeActivePanel()
    {
        this.gameObject.SetActive(false);
        TravelDestination = "";
    }

    public void ActivePanel()
    {
        this.gameObject.SetActive(true);
    }

    #endregion
}
