﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*  Added the new UGUI!
 *  Just active the Panel with the Instance, pass the tag to change the text, we use the 
 *  same string passed to update the TravelDestination!
 */

public class NavigationPrompt : MonoBehaviour
{
    //private bool showDialog;

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (NavigationManager.CanNavigate(this.tag))
        {
            if (col.gameObject.CompareTag("Player"))
            {
                //showDialog = true;   
                NavigationUIManager.Instance.ActivePanel();
                NavigationUIManager.Instance.SetUpText(this.tag);
            }        
        }
    }

    private void OnCollisionExit2D(Collision2D col)
    {
        //showDialog = false;
        NavigationUIManager.Instance.DeActivePanel();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        
        if (NavigationManager.CanNavigate(this.tag))
            if (col.CompareTag("Player"))
            {
                //showDialog = true;
                NavigationUIManager.Instance.ActivePanel();
                NavigationUIManager.Instance.SetUpText(this.tag);
            }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        //showDialog = false;
        NavigationUIManager.Instance.DeActivePanel();
    }



    //TODO!
    //Using old GUI just to follow the book!
    //void OnGUI()
    //{
    //    if (showDialog)
    //    {
    //        //Layout start
    //        GUI.BeginGroup(new Rect(Screen.width / 2 - 150, 50, 300, 250));

    //        //The menu background box
    //        GUI.Box(new Rect(0, 0, 300, 250), "" );

    //        //Information text, accessing the Navigation Manager to more detailed info.
    //        GUI.Label(new Rect(15, 10, 300, 68), "Do you want to travel to " + NavigationManager.GetRouteInfo(this.tag) + "?");

    //        //Player wants to leave this location.
    //        if (GUI.Button(new Rect(55, 100, 180, 40), "Travel"))
    //        {
    //            showDialog = false;
                
    //            //Use the NavigationManager to travel to the right place.
    //            NavigationManager.NavigateTo(this.tag);

    //        }

    //        //Player wants to stay.
    //        if (GUI.Button(new Rect(55, 150, 180, 40), "Stay"))
    //        {
    //            showDialog = false;
    //        }

    //        //Layout end
    //        GUI.EndGroup();
    //    }
    //}
}
