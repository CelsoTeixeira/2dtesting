﻿using System.Linq;
using UnityEngine;
using System.Collections.Generic;

/*Static Script
 *  This sits in the background, only exists once in the game, and is accessible from anywhere.
 *  This pattern is useful for fixed information taht isn't attached to anything.
 *  It just sits in the background waiting to be queried.
 */

public static class NavigationManager
{
    public static Dictionary<string, string> RouteInformation = new Dictionary<string, string>()
    {
        {"World", "The big bad world"},
        {"Cave", "The deep dark cave"},
        {"Town Hall", "The main building in the city"},
        {"Armor Weapons Shop", "The warrior shop"},
        {"Robs Rods Shop", "The mages shop"},
        {"Potions Shop", "Keep alive, fight strong! Potions Shop"}
    };

    public static string GetRouteInfo(string destination)
    {
        return RouteInformation.ContainsKey(destination) ? RouteInformation[destination] : null;
        //For reference on this conditional operator: https://msdn.microsoft.com/pt-br/library/ty67wk28.aspx
    }

    public static bool CanNavigate(string destination)
    {
        return true;
    }

    public static void NavigateTo(string destination)
    {
        //Application.LoadLevel(destination);
    }
}