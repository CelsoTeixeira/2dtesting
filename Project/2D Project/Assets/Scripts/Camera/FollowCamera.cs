﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour
{
    #region Public Fields

    //Distance in the x axis the player can move before the camera follows.
    public float xMargin = 1.5f;

    //Distance in the y axis the player can move before the camera follows.
    public float yMargin = 1.5f;

    //How smoothly the camera catches up with its target movement in the x axis.
    public float xSmooth = 1.5f;

    //How smoothly the camera catches up with its target movement in the y axis.
    public float ySmooth = 1.5f;


    //Reference to the player's transform.
    public Transform PlayerTransform;

    #endregion

    #region Private Fields
    //The maximun x and y coordinates the camera can have.
    private Vector2 maxXAndY;

    //The minumun x and y coordinates the camera can have.
    private Vector2 minXAndY;

    #endregion

    #region Mono

    void Awake()
    {
        SetUpCameraBounds();
    
        CheckPlayerTransform();
    }

    void FixedUpdate()
    {
        //By default the target x and y coordinates of the camera are it's current x and y coordinates.
        float targetX = transform.position.x;
        float targetY = transform.position.y;

        //If the player has moved beyond the x margin...
        if (CheckXMargin())
        {
            //The target x coordiante should be a Lerp between the camera's current x and the player's current x.
            targetX = Mathf.Lerp(transform.position.x, PlayerTransform.position.x, xSmooth*Time.fixedDeltaTime);
        }

        //If the player has moved beyond the y margin...
        if (CheckXMargin())
        {
            //The target x coordiante should be a Lerp between the camera's current y and the player's current y.
            targetY = Mathf.Lerp(transform.position.y, PlayerTransform.position.y, ySmooth * Time.fixedDeltaTime);
        }

        //The target x and y coordinates should not be larger than the maximun or smaller than the minimun.
        targetX = Mathf.Clamp(targetX, minXAndY.x, maxXAndY.x);
        targetY = Mathf.Clamp(targetY, minXAndY.y, maxXAndY.y);

        //Set the camera's position to the target position with the same z component.
        transform.position = new Vector3(targetX, targetY, transform.position.z);
    }

    #endregion

    #region Private Methods

    private void CheckPlayerTransform()
    {
        //Setting the reference for the player if it's null.
        if (PlayerTransform == null)
        {
            PlayerTransform = GameObject.FindGameObjectWithTag("Player").transform;

            if (PlayerTransform == null)
                Debug.Log("Player not found", this.gameObject);
        }
    }

    private void SetUpCameraBounds()
    {
        //Get the bounds for the background texture - world size
        var backgroundBounds = GameObject.FindGameObjectWithTag("Background").GetComponent<Renderer>().bounds;

        //Get the viewable bounds of the camera in world coordinates.
        var camTopLeft = GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0, 0, 0));
        var camBottomRight = GetComponent<Camera>().ViewportToWorldPoint(new Vector3(1, 1, 0));

        //Automatically set the min and max values.
        minXAndY.x = backgroundBounds.min.x - camTopLeft.x;
        maxXAndY.x = backgroundBounds.max.x - camBottomRight.x;
    }

    private bool CheckXMargin()
    {
        //Return true if the distance between the camera and the player in the x axis is greater than the x margin.
        return Mathf.Abs(transform.position.x - PlayerTransform.position.x) > xMargin;
    }

    private bool CheckYMargin()
    {
        return Mathf.Abs(transform.position.y - PlayerTransform.position.y) > yMargin;
    }

    #endregion

}
