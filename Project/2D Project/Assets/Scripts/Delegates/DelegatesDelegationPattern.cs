﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class DelegatesDelegationPattern : MonoBehaviour 
{
    public class Worker
    {
        List<string> WorkCompletedfor = new List<string>();

        public void DoSomething(string ManagerName, Action myDelegate)
        {
            //Audits that work was done for the which manager
            WorkCompletedfor.Add(ManagerName);

            //Begin work
            myDelegate();
        }
    }

    public class Manager
    {
        private Worker myWorker = new Worker();

        public void PieceOfWork1()
        {
            //A piece of very long tedious work
        }

        public void PieceOfWork2()
        {
            //You guessed it, yet more tedious work
        }

        public void DoWork()
        {
            //Send work to do job1
            myWorker.DoSomething("Manager1", PieceOfWork1);
            
            //Sende work to do jog 2
            myWorker.DoSomething("Manager1", PieceOfWork2);
        }

        public void DoWork2()
        {
            Worker myWorker = new Worker();
   
            //Send worker to do job 1
            myWorker.DoSomething("Manager1", () =>
            {
                //A piece of very long tedious work
            });

            myWorker.DoSomething("Manager1", () =>
            {
                //You guessed it, yet more tedious work
            });
        }

    }

}
