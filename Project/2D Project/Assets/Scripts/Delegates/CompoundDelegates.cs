﻿using UnityEngine;
using System.Collections;

/*  You can assign multiple functions to a single delegate.
 *  This feature is very handy when you want to chain several common functions togheter instead of one.
 *
 */

public class CompoundDelegates 
{
    void DoWork()
    {
        DoJob1();
        DoJob2();
        DoJob3();
    }

    private void DoJob1()
    {
        //Do some filling;
    }

    private void DoJob2()
    {
        //Make coffee for the office.
    }

    private void DoJob3()
    {
        //Stick it to the man.
    }

    //An more intelligent worker.
    public class WorkerManager : MonoBehaviour
    {
        //Worker Manager delegate
        private delegate void MyDelegatetHook();
        private MyDelegatetHook ActionsToDo;

        public string WorkerType = "Peon";

        //OnStartUp. assing jobs to the worker
        void Start()
        {
            //Peons get lots of work to do
            if (WorkerType == "Peon")
            {
                ActionsToDo += DoJob1;
                ActionsToDo += DoJob2;
            }
            else
            {
                ActionsToDo += DoJob3;
            }
        }

        void Update()
        {
            //Do the actions set on ActionsToDo.
            ActionsToDo();
        }

        private void DoJob1()
        {
            //Do some filling
        }

        private void DoJob2()
        {
            //Make coffee for the office
        }

        private void DoJob3()
        {
            //Play golf
        }
    }

}
