﻿using UnityEngine;
using System.Collections;

/*  The configurable method pattern: is used when a piece of work or function is passed to another method to be used to complete a task.
 *  This pattern is usually used where different pieces of code can perform a common task in unique ways, such as walking, running, or patrolling.
 *  All these tasks can be the default behaviours of a character.
 * 
 */

public class Delegates : MonoBehaviour
{
    //Define a delegate method signature
    private delegate void RobotAction();

    //Private property for delegate use
    private RobotAction myRobotAction;

    void Start()
    {
        //Set the default method for the delegate
        myRobotAction = RobotWalk;
    }

    void Update()
    {
        //Run the selected delegate method on update
        myRobotAction();
    }

    //Public method to tell the robot to walk
    public void DoRobotWalk()
    {
        //Set the delegate method to the walk function
        myRobotAction = RobotWalk;
    }

    void RobotWalk()
    {
        Debug.Log("Robot Walking");
    }

    //Public method to tell the robot to run
    public void DoRobotRun()
    {
        //set the delegate method to the run function
        myRobotAction = RobotRun;
    }

    void RobotRun()
    {
        Debug.Log("Robot running");
    }
}
