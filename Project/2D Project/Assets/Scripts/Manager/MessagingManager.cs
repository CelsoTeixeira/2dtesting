﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class MessagingManager : MonoBehaviour
{
    #region Singleton Pattern
    //Static singleton property
    public static MessagingManager Instance { get; private set; }

    private void SingletonSetUp()
    {
        if (Instance != this && Instance != null)
            Destroy(gameObject);

        Instance = this;
    }
    #endregion

    //Public property for manager
    private List<Action> subscribers = new List<Action>();

    void Awake()
    {
        SingletonSetUp();
    }

    #region Public Access

    //The subscribe method for manager
    public void Subscribe(Action subscriber)
    {
        Debug.Log("Subscriber registered");
        subscribers.Add(subscriber);
    }

    //The unsubscribe method for the manager
    public void UnSubscribe(Action subscriber)
    {
        Debug.Log("Subscriber removed");
        subscribers.Remove(subscriber);
    }

    //Clear subscriber method for manager
    public void ClearAllSubscribers()
    {
        subscribers.Clear();
    }

    //Broadcast method to let all the subscriber know what happened
    public void Broadcast()
    {
        Debug.Log("Broadcast requested, No of subscribers = " + subscribers.Count);
        
        //Simply loop through all the subscribers and notify them using their delegates.
        foreach (var subscriber in subscribers)
        {
            subscriber();
        }
    }

    #endregion
}
